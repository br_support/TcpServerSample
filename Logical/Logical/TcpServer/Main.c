#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	// Insert code here
}

void _CYCLIC ProgramCyclic(void)
{
		switch(Server.State)
 		{
		case 0:// Open Ethernet Interface
			Server.TcpOpen_0.enable = 1;	
			Server.TcpOpen_0.pIfAddr = 0;  // Listen on all TCP/IP Interfaces
			Server.TcpOpen_0.port = LISTEN_PORT;  // Port to listen
			Server.TcpOpen_0.options = 0;	
			TcpOpen(&Server.TcpOpen_0);  // Call the Function
						
			if(Server.TcpOpen_0.status == 0) // TcpOpen successfull
			{
				Server.State = 5;	
			}
			else if(Server.TcpOpen_0.status == ERR_FUB_BUSY)  // TcpOpen not finished -> redo 			
			{// Busy 
			} 	
			else
			{// Goto Error Step 
				if( Server.TcpOpen_0.status == tcpERR_ALREADY_EXIST) Server.State = 50;
				else  Server.State = 100;
			}
		break;
		
		
		case 5:
			Server.linger_opt.lLinger = 0; // linger Time = 0 
			Server.linger_opt.lOnOff = 0; // linger Option OFF 
				 
		 	Server.TcpIoctl_0.enable = 1;
			Server.TcpIoctl_0.ident = Server.TcpOpen_0.ident; // Connection Ident from AsTCP.TCP_Open 
			Server.TcpIoctl_0.ioctl = tcpSO_LINGER_SET; // Set Linger Options 
			Server.TcpIoctl_0.pData = (UDINT)&Server.linger_opt;
			Server.TcpIoctl_0.datalen = sizeof(Server.linger_opt);
   			TcpIoctl(&Server.TcpIoctl_0);	    
		
			if(Server.TcpIoctl_0.status == 0)  // TcpIoctl successfull 
				Server.State = 10;											
			else if(Server.TcpIoctl_0.status == ERR_FUB_BUSY)  // TcpIoctl not finished -> redo 				
			{}  // Busy 
			else  // Goto Error Step 
				Server.State = 100;
		
		break;
		
		case 10: // Wait for Client Connection
			Server.TcpServer_0.enable = 1;
			Server.TcpServer_0.ident = Server.TcpOpen_0.ident;  // Connection Ident from AsTCP.TCP_Open 
			Server.TcpServer_0.backlog = 1;  // Number of clients waiting simultaneously for a connection
			Server.TcpServer_0.pIpAddr = (UDINT)Server.client_address;  // Where to write the client IP-Address
			TcpServer(&Server.TcpServer_0);  // Call the Function
			
			if(Server.TcpServer_0.status == 0)// Status = 0 if an client connects to server 
				Server.State = 15;
			else if(Server.TcpServer_0.status == ERR_FUB_BUSY)  // TcpServer not finished -> redo 			
			{}  // Busy 
			else  // Goto Error Step 
				Server.State = 100;
		break;	
			
		case 15:
		 	Server.TcpIoctl_0.enable = 1;
			Server.TcpIoctl_0.ident = Server.TcpServer_0.identclnt; // Connection Ident from AsTCP.TCP_Server 
			Server.TcpIoctl_0.ioctl = tcpSO_LINGER_SET;  // Set Linger Options 
			Server.TcpIoctl_0.pData = (UDINT)&Server.linger_opt;
			Server.TcpIoctl_0.datalen = sizeof(Server.linger_opt);
   			TcpIoctl(&Server.TcpIoctl_0);	    
		
			if(Server.TcpIoctl_0.status == 0 )// TcpIoctl successfull 
			{
				Server.ReceiveTimeout = 0;
				Server.State = 20;
			}
			else if(Server.TcpIoctl_0.status == ERR_FUB_BUSY) // TcpIoctl not finished -> redo 				
			{}  // Busy  
			else  // Goto Error Step 
				Server.State = 100;
		break;
		
		
		case 20: // Wait for Data
			Server.TcpRecv_0.enable = 1;
			Server.TcpRecv_0.ident = Server.TcpServer_0.identclnt;  // Client Ident from AsTCP.TCP_Server 	
			Server.TcpRecv_0.pData	= (UDINT)dataBuffer;  // Where to store the incoming data 
			Server.TcpRecv_0.datamax = sizeof(dataBuffer);  // Lenght of data buffer 
			Server.TcpRecv_0.flags = 0;
		 	TcpRecv(&Server.TcpRecv_0);  // Call the Function
		
			if(Server.TcpRecv_0.status == 0)
			{  // Data received 
				dataBufferLength = Server.TcpRecv_0.recvlen;
				if(Server.TcpRecv_0.recvlen==0) 
					Server.State = 40;
				else 
				{
					Server.State = 30;
					Server.ReceiveTimeout = 0;
				}
			}
			else if(Server.TcpRecv_0.status == tcpERR_NO_DATA)
			{  // No data received - wait
				if(++Server.ReceiveTimeout >= 50) 
				{
					Server.State = 40;
				}
			}
			else if(Server.TcpRecv_0.status == ERR_FUB_BUSY)  // TcpRecv not finished -> redo 					
			{}  // Busy 
			else if( Server.TcpRecv_0.status == tcpERR_NOT_CONNECTED)
				Server.State = 40;
			else  // Goto Error Step 
				Server.State = 100;
			
		break;
		
		case 30:	// Send Data back to Client
			Server.TcpSend_0.enable = 1;				
			Server.TcpSend_0.ident = Server.TcpServer_0.identclnt;  // Client Ident from AsTCP.TCP_Server 	
			Server.TcpSend_0.pData = (UDINT)dataBuffer;  // Which data to send 
			Server.TcpSend_0.datalen = dataBufferLength;  // Lenght of data to send 					
			Server.TcpSend_0.flags = 0;
			TcpSend(&Server.TcpSend_0);  // Call the Function
				
			if( Server.TcpSend_0.status == 0)// Data sent 
				Server.State = 20;					   
			else if (Server.TcpSend_0.status == ERR_FUB_BUSY)  // TcpSend not finished -> redo 						
			{}  // Busy 
			else if (Server.TcpSend_0.status == tcpERR_NOT_CONNECTED)
				Server.State = 40;
			else // Goto Error Step 
				Server.State = 100;
		break;	
		
		
		case 40:
			Server.TcpClose_0.enable = 1;
			Server.TcpClose_0.ident = Server.TcpServer_0.identclnt;
			Server.TcpClose_0.how = 0;; // tcpSHUT_RD OR tcpSHUT_WR;
			TcpClose(&Server.TcpClose_0);
			
			if( Server.TcpClose_0.status == 0)
   				Server.State = 10;
			else if(Server.TcpClose_0.status == ERR_FUB_BUSY)  // TcpClose not finished -> redo 	
			{}  // Busy 
			else  // Goto Error Step 
				Server.State = 100;
			
		break;
		
		case 50: 
			Server.TcpClose_0.enable = 1;
			Server.TcpClose_0.ident = Server.TcpOpen_0.ident;
			Server.TcpClose_0.how = 0; //tcpSHUT_RD OR tcpSHUT_WR;
			TcpClose(&Server.TcpClose_0);
			
			if (Server.TcpClose_0.status == 0)
   				Server.State = 0;
			else if(Server.TcpClose_0.status == ERR_FUB_BUSY)  // TcpClose not finished -> redo 	
			{}  // Busy  
			else   // Goto Error Step 
				Server.State = 100;
						
	    
		case 100:  // Here some error Handling has to be implemented
		
		break;
	}//switch
		

}

void _EXIT ProgramExit(void)
{
	// Insert code here 

}

