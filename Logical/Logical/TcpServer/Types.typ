
TYPE
	TcpServer_type : 	STRUCT  (*TCP Server Variables*)
		State : UINT; (*TCP Server Step Variable*)
		ReceiveTimeout : UDINT; (*receive timeout*)
		client_address : STRING[20]; (*Address of the client connection to the Server*)
		TcpOpen_0 : TcpOpen; (*AsTCP.TcpOpen FUB*)
		TcpServer_0 : TcpServer; (*AsTCP.TcpServer FUB*)
		TcpRecv_0 : TcpRecv; (*AsTCP.TcpRecv FUB*)
		TcpSend_0 : TcpSend; (*AsTCP.TcpSend FUB*)
		TcpClose_0 : TcpClose; (*AsTCP.TcpClose FUB*)
		TcpIoctl_0 : TcpIoctl; (*AsTCP.TcpIoctl*)
		linger_opt : tcpLINGER_typ; (*AsTCP.tcpLINGER_typ*)
	END_STRUCT;
END_TYPE
